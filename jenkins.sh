#!/bin/bash

if [ -z $vm_name ]
else 
	echo "Virtual mashine name not set. Exit"
	exit 1;
fi 


if [ -z $report_patch ]
else 
	echo "Report patch not set. Exit"
	exit 1;
fi 

#prepare folder structure
echo "prepare folder structure"
echo "prepare folder structure >> clenup folders"
rm -rf /vm/$vm_name
rm -rf $report_patch

echo "prepare folder structure >> create folders"
mkdir /vm $report_patch
mkdir /vm/$vm_name /vm/$vm_name/dweb

#build script  evn


cd $JENKINS_HOME/workspace/$JOB_NAME/vm
sed -i  "s/name-of-vm/$vm_name/g"   Vagrantfile

echo "Update vagrant box"
vagrant box update

echo "Start vagrant vm no-provision"
vagrant up --no-provision 

echo "Create snapshot"
vagrant snapshot push

echo "Executing test script on VM"
vagrant provision #| logger
cp /vm/$vm_name/dweb/vm/report.xml $report_patch

echo 'Revert changes'
vagrant snapshot pop --no-provision 

echo 'Report location'  /vm/$vm_name/dweb/vm/report.xml 



