#!/bin/bash

if [ ! -d env ]; then
        
	virtualenv /tmp/env
else 
	mkdir /tmp/env
	virtualenv /tmp/env
fi

source /tmp/env/bin/activate
pip install pytest
py.test --junit-xml=/vagrant/report.xml /tmp/test.py
