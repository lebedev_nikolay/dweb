#!/bin/bash
git_adress=$3;
branch=$4;
vm_name=$1;
report_patch=$2;

#Example of script execution script.sh ubuntu /tmp/report https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/dweb.git master  
#parametr  check 1,3,4 mandatory rule. 2 set default if not exist

if [ -n "$3" ] 
	then echo "git url parametr syntaxis is good"
else 
	echo "git url not set. Example of script execution ./script.sh ubuntu /tmp/report https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/dweb.git master   "
	exit 1;
fi 

if [ -n "$4" ]
	then echo "branch parametr syntaxis is good"
else 
	echo "branch not set in parametr. use default branch master"
	branch="master"
fi 

if [ -n "$1" ]
	then echo "vm name parametr syntaxis is good"
else 
	echo "VM name not set. Example of script execution ./script.sh ubuntu /tmp/report https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/dweb.git master   "
	exit 1;
fi 

if [ -n "$2" ]
	then echo "report patch parametr syntaxis is good"
else 
	echo "Report patch not set. Example of script execution ./script.sh ubuntu /tmp/report https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/dweb.git master   "
	exit 1;
fi

#prepare folder structure
echo "prepare folder structure"
echo "prepare folder structure >> clenup folders"
rm -rf /vm/$vm_name
rm -rf $report_patch

echo "prepare folder structure >> create folders"
mkdir /vm $report_patch
mkdir /vm/$vm_name /vm/$vm_name/dweb

#build script  evn
echo "build script  evn >> create folders and get script from git"
cd /vm/$vm_name
git clone -b $branch https://lebedev_nikolay@bitbucket.org/lebedev_nikolay/dweb.git
cd dweb
chmod +x -R  *.sh
cd vm
sed -i  "s/name-of-vm/$vm_name/g"   Vagrantfile

echo "Update vagrant box"
vagrant box update

echo "Start vagrant vm no-provision"
vagrant up --no-provision 

echo "Create snapshot"
vagrant snapshot push

echo "Executing test script on VM"
vagrant provision #| logger
cp /vm/$vm_name/dweb/vm/report.xml $report_patch

echo 'Revert changes'
vagrant snapshot pop --no-provision 

echo 'Report location'  /vm/$vm_name/dweb/vm/report.xml 




